/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package com.enset.Enset.entities;

import java.io.Serializable;
import java.util.Collection;
import javax.persistence.Entity;
import javax.persistence.Id;
import javax.persistence.JoinTable;
import javax.persistence.ManyToMany;
import javax.persistence.Table;
@Entity
@Table(name="users")

/**
 *
 * @author jahaelle
 */
public class User implements Serializable{
    @Id
    private String ursername;
    private String password;
    private boolean actived;
    @ManyToMany
     @JoinTable(name="USERS_ROLES")       
    private Collection <Role> roles;
    public String getUrsername() {
        return ursername;
    }

    public String getPassword() {
        return password;
    }

    public Collection<Role> getRoles() {
        return roles;
    }

    public void setUrsername(String ursername) {
        this.ursername = ursername;
    }

    public void setPassword(String password) {
        this.password = password;
    }

    public void setRoles(Collection<Role> roles) {
        this.roles = roles;
    }

    public User(String ursername, String password, boolean actived) {
        this.ursername = ursername;
        this.password = password;
        this.actived = actived;
    }

   

    public User() {
    }
    
    
    
}
