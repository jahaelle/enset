package com.enset.Enset;

import com.enset.Enset.dao.EtudiantRepository;
import com.enset.Enset.entities.Etudiant;
import java.text.DateFormat;
import java.text.ParseException;
import java.text.SimpleDateFormat;
import static java.time.Clock.system;
import java.util.List;
import org.springframework.boot.SpringApplication;
import org.springframework.boot.autoconfigure.SpringBootApplication;
import org.springframework.context.ApplicationContext;

@SpringBootApplication
public class EnsetApplication {

	public static void main(String[] args) throws ParseException {
ApplicationContext ctx= SpringApplication.run(EnsetApplication.class, args);

EtudiantRepository etudiantRepository = ctx.getBean(EtudiantRepository.class);
            DateFormat df = new SimpleDateFormat("yyyy-MM-dd");
etudiantRepository.save(new Etudiant("fomo","jahaelle",df.parse("1997-09-27")));


List<Etudiant> etds=etudiantRepository.findAll();
etds.forEach(e->System.out.println(e.getNom()));
	}

}
